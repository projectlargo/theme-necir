<?php
/**
 * The homepage template
 */
get_header();

/*
 * Collect post IDs in each loop so we can avoid duplicating posts
 * and get the theme option to determine if this is a two column or three column layout
 */
global $shown_ids;
$layout = of_get_option('homepage_layout');
$tags = of_get_option ('tag_display');
?>

<div id="content" class="stories span8 <?php echo $layout; ?>" role="main">

		<div id="homepage-featured" class="row-fluid clearfix">
			<div class="top-story span12">
			<?php
				$topstory = largo_get_featured_posts( array(
					'tax_query' => array(
						array(
							'taxonomy' 	=> 'prominence',
							'field' 	=> 'slug',
							'terms' 	=> 'top-story'
						)
					),
					'showposts' => 1
				) );
				if ( $topstory->have_posts() ) :
					while ( $topstory->have_posts() ) : $topstory->the_post(); $shown_ids[] = get_the_ID();

						if( $has_video = get_post_meta( $post->ID, 'youtube_url', true ) ) { ?>
							<div class="embed-container">
								<iframe src="http://www.youtube.com/embed/<?php echo substr(strrchr( $has_video, "="), 1 ); ?>?modestbranding=1" frameborder="0" allowfullscreen></iframe>
							</div>
						<?php } else { ?>
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'large' ); ?></a>
						<?php } ?>

						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					    <h5 class="byline"><?php largo_byline(); ?></h5>
					    <?php largo_excerpt( $post, 4, false ); ?>
					    <?php if ( largo_post_in_series() ):
							$feature = largo_get_the_main_feature();
							$feature_posts = largo_get_recent_posts_for_term( $feature, 1, 1 );
							if ( $feature_posts ):
								foreach ( $feature_posts as $feature_post ): ?>

									<h4 class="related-story"><?php _e('RELATED:', 'largo'); ?> <a href="<?php echo esc_url( get_permalink( $feature_post->ID ) ); ?>"><?php echo get_the_title( $feature_post->ID ); ?></a></h4>
								<?php endforeach;
							endif;
						endif;
					endwhile;
				endif; // end top story ?>
			</div>
		</div>

		<div id="homepage-bottom-necir" class="row-fluid clearfix">

			<div class="span8">

				<?php
				// sticky posts box if this site uses it
				if ( of_get_option( 'show_sticky_posts' ) ) {
					get_template_part( 'home-part', 'sticky-posts' );
				}

					$args = array(
						'paged'			=> $paged,
						'post_status'	=> 'publish',
						'posts_per_page'=> 10,
						'post__not_in' 	=> $shown_ids
						);
					if ( of_get_option('num_posts_home') )
						$args['posts_per_page'] = of_get_option('num_posts_home');
					if ( of_get_option('cats_home') )
						$args['cat'] = of_get_option('cats_home');
					$query = new WP_Query( $args );

					if ( $query->have_posts() ) {
						while ( $query->have_posts() ) : $query->the_post();
							//if the post is in the array of post IDs already on this page, skip it
							if ( in_array( get_the_ID(), $shown_ids ) ) {
								continue;
							} else {
								$shown_ids[] = get_the_ID();
								get_template_part( 'partials/content', 'home' );
							}
						endwhile;
						largo_content_nav( 'nav-below' );
					} else {
						get_template_part( 'partials/content', 'not-found' );
					} ?>

			</div>

			<div id="left-rail" class="span4">
			<?php if ( ! dynamic_sidebar( 'homepage-left-rail' ) ) : ?>
				<p><?php _e('Please add widgets to this content area in the WordPress admin area under appearance > widgets.', 'largo'); ?></p>
			<?php endif; ?>
			</div>

	</div>


</div><!-- #content-->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
